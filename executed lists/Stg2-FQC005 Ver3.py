import os
import openpyxl

SourcePath='C:\\Users\\ornitcg\\Desktop\\PythonExc\\Stage2-FQC005 -Ver2\\SourceFiles'
TargetPath='C:\\Users\\ornitcg\\Desktop\\PythonExc\\Stage2-FQC005 -Ver2\\NewFiles'
SourceList='Stage2MaterialsList-07022017-2.xlsx'  ##  ****change list name****
os.chdir(SourcePath)
rawWB= openpyxl.load_workbook(SourceList)
rawWs= rawWB.get_sheet_by_name('ListSheet')  #list of raw materials for FQC004

for i in range(2,90):    #range of source   ****change range*****
    os.chdir(SourcePath)
    rawWB= openpyxl.load_workbook(SourceList)
    rawWs= rawWB.get_sheet_by_name('ListSheet')  #list of raw materials for FQC004
    FQCtmp= openpyxl.load_workbook('FQC005 Ver 002 Antigen for Stage 2 Production Preparation Form.xlsx')
    FQCtmpsh = FQCtmp.get_sheet_by_name('Sheet1')
   # print(FQCtmp.get_sheet_names())
    
    FQCtmpsh['A8']=rawWs.cell(row=i,column=1).value  # BiochemicalName
    FQCtmpsh['B8']=rawWs.cell(row=i,column=6).value  # CatNumber
    FQCtmpsh['C8']=rawWs.cell(row=i,column=5).value  # LotNumber
    FQCtmpsh['D8']=rawWs.cell(row=i,column=3).value   # Stg1ItemID   
    FQCtmpsh['E8']=rawWs.cell(row=i,column=4).value   # Stge1Name
    FQCtmpsh['A24'].value ="Planned By:  Name  ORNIT"
    FQCtmpsh['B9']=rawWs.cell(row=i,column=7).value   # serialNumber of aliquoting
    FQCtmpsh['C10']=rawWs.cell(row=i,column=8).value   # SOP Version
    FQCtmpsh['B13']=rawWs.cell(row=i,column=10).value   # SourceVials Location
    FQCtmpsh['B14']=rawWs.cell(row=i,column=11).value   # Stge1Vol
    FQCtmpsh['B15']=rawWs.cell(row=i,column=12).value   # Instructions
    FQCtmpsh['B16']=rawWs.cell(row=i,column=13).value   # BufferName
    FQCtmpsh['B17']=rawWs.cell(row=i,column=14).value   # bufferVol
    FQCtmpsh['D16']=rawWs.cell(row=i,column=20).value   # bufferItemID
    FQCtmpsh['B18']=rawWs.cell(row=i,column=15).value   # stage2Conc
    FQCtmpsh['B19']=rawWs.cell(row=i,column=16).value   # Vol/Vial
    FQCtmpsh['B20']=rawWs.cell(row=i,column=17).value   # NOofAliquots
    FQCtmpsh['B22']=rawWs.cell(row=i,column=9).value   # Temperature
    FQCtmpsh['B23']=rawWs.cell(row=i,column=19).value   # Stage2ItemID
    FQCtmpsh['E23']=rawWs.cell(row=i,column=18).value   # Stge2Name
     
   # if len(FQCtmpsh['F8'].value)>20:
    #    FQCtmpsh['F8'].font.size=10
    os.chdir(TargetPath)
    FQCtmp.save(str(rawWs.cell(row=i,column=1).value) +' ItemID# '+str(rawWs.cell(row=i,column=3).value) + ' Prep#' + str(FQCtmpsh['B9'].value) + ' FQC005 Ver 002 Antigen for Stage 2 Production Preparation Form.xlsx')
